# MktApiFake
Modulo de instalação
Requistos:
Nodejs > 8.7
NPM
Angular Cli(npm install angular-cli -g)

Execute npm install

Criar pasta data/db para o mongo 

Criar arquivo de host na pasta src-api/core
```json
[
  {
    "host": "mktfake.localhost",
    "folderName": "default"
  },
  {
    "host": "skyhub.mktfake.localhost",
    "folderName": "skyhub"
  }
]
```
npm run startmongo (start mongo local windows)

npm start (start frontend dashboard e backend nodejs) 

primeria vez que rodar deve-se executar o comando npm run mongorestore para criar as collections no mongo(mongo deve estar startado)

http://localhost:4800  dashboard
