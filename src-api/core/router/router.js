/**
 * Created by alexandre on 30/08/17.
 */
var express = require('express');
var glob = require("glob");
var _ = require("lodash");
var path = require("path");
module.exports = function (rout, endpoint) {
  var controllers = function(){
   var pathController = [];
    var controller = glob.sync(__root + '/src-api/application/endpoints/' + endpoint + '/controllers/*.js');
    if (controller) {
          _.forEach(controller, function (r) {
            var obj = require(r);
            var name = path.basename(r);
            pathController.push({
              name: name.replace("-controller", "").replace(".js", ""),
              controller: obj
            });
          });
        }
    return pathController;
  }

  var rout = rout.split('/');
  var ctr = _.find(controllers(), function (r) {
    return  r.name == rout[1];
  });
  if(ctr){
    var metod = (rout[2]) ? rout[2] : 'index';
    return (ctr.controller[metod]) ? ctr.controller[metod] : function(req, res){res.json({error:'Rota não encontrada'})};
  }else{
    return function(req, res){
      res.status(404).json({ error:'recurso que não existe'})
    }
  }
}


