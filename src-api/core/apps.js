/*
              Configurações express
 */
var path = require('path');
var express = require('express');
const bodyParser = require('body-parser');
var webpack = require('webpack');
var cookieParser = require('cookie-parser');
var cors = require('cors');
var swig = require('swig');
var _ = require("lodash");
var MongoClient = require('mongodb').MongoClient;
var swagger = require("swagger-node-express");
var fs = require('fs');
var obj;
var appsArr = [];
var portmongo = process.env.PORTMONGO | 23456;
var data = fs.readFileSync(__root + '/src-api/core/hosts.json', 'utf8');
var json = JSON.parse(data);
  _.forEach(json, function (r) {
    var app = express();
    app.domain = r.host;
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(cookieParser());
    swig.setDefaults({ varControls: ['<%', '%>'] });
    app.engine('html', swig.renderFile);
    app.set('view engine', 'html');
    var routerapi = require(__root + '/src-api/application/endpoints/' + r.folderName + '/routers/router.api.config.js');
    app.use('*', cors());

    MongoClient.connect("mongodb://localhost:" + portmongo + "/", { promiseLibrary: Promise }, (err, db) => {
      if (err) {
        logger.warn(`Failed to connect to the database. ${err.stack}`);
      }
      app.use("*", function (req, res, next) {
        req.db = db.db("local");
        next();
      });
      app.use(routerapi);
      swagger.setAppHandler(app);
    });

    appsArr.push({ host: r.host, app: app });
 });


module.exports = appsArr;







