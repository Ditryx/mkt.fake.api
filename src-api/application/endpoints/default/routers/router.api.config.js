var express = require('express');
var Router = require(__root + "/src-api/core/router/router.js");
const router = express.Router();

//registro de rotas  /Controller/Action, 'endpoint'
router.get('/dashboard', Router('/dashboard', 'default'));

//skyhub consumer front
//Seller Skyhub
router.get('/skyhub/sellers', Router('/skyhub/sellers', 'default'));
router.get('/skyhub/sellers/:id', Router('/skyhub/editSeller', 'default'));
router.post('/skyhub/sellers/create', Router('/skyhub/createSeller', 'default'));
router.delete('/skyhub/sellers/delete/:id', Router('/skyhub/deleteSeller', 'default'));


//Order Skyhub
router.get('/skyhub/orders', Router('/skyhub/orders', 'default'));
router.get('/skyhub/orders/model', Router('/skyhub/modelOrder', 'default'));
router.get('/skyhub/orders/:id', Router('/skyhub/editOrder', 'default'));
router.post('/skyhub/orders/create', Router('/skyhub/createOrder', 'default'));
router.post('/skyhub/orders/post', Router('/skyhub/post', 'default'));


router.delete('/skyhub/orders/delete/:id', Router('/skyhub/deleteOrder', 'default'));
module.exports = router;

