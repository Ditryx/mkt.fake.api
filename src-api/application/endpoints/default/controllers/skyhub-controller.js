var fs = require('fs');
var _ = require('lodash');
var mongodb = require('mongodb');
var request = require('request');
module.exports = {
  orders: async function (req, res, next) {
    var params = req.query;
    params.page = (!params.page) ? 0 : parseInt(params.page);
    params.per_page = (!params.per_page) ? 30 : parseInt(params.per_page);
    var filter = {};
    if (typeof req.query.apiKey != "undefined") {
      filter["x-api-key"] = req.query.apiKey
    };
    var count = await req.db.collection("skyhub_orders").count();

    req.db.collection("skyhub_orders").aggregate([{
      $lookup: {
        from: 'skyhub_sellers',
        localField: 'x-api-key',
        foreignField: 'x-api-key',
        as: 'seller'
      }
    }
    ]).skip(params.page * params.per_page).limit(params.per_page).toArray(function (err, result) {
      res.json({ list: result, count: count });
    });
  },
  //list
  sellers: function (req, res, next) {
    req.db.collection("skyhub_sellers").find({}).toArray(function (err, result) {
      res.json(result);
    });
  },
  //add e edit
  editSeller: function (req, res, next) {
    var params = req.params;
    if (params.id) {
      req.db.collection("skyhub_sellers").findOne({ _id: new mongodb.ObjectID(params.id) }, function (err, result) {
        res.json({ success: true, seller: result });
      });
    } else {
      res.json({ success: true });
    }
  },
  //post create update
  createSeller: function (req, res, next) {
    var body = req.body;
    body.block = false;
    if (body._id != null) {
      var id = body._id;
      delete body._id;
      body.block = false;
      req.db.collection("skyhub_sellers").updateOne({ _id: new mongodb.ObjectID(id) }
        , { $set: body }, function (err, result) {
          if (err) {
            res.json({ success: false });
          } else {
            res.json({ success: true });
          }
        });
    } else {
      delete body._id;
      req.db.collection("skyhub_sellers").insertOne(body,
        function (err, result) {
          console.log(err);
          if (err) {
            res.json({ success: false });
          } else {
            res.json({ success: true });
          }
        });
    }
  },
  //delete seller
  deleteSeller: function (req, res, next) {
    var id = req.params.id;
    console.log(id);
    req.db.collection("skyhub_sellers").deleteOne({ _id: new mongodb.ObjectID(id), block: false }
      , function (err, result) {
        if (err) {
          res.json({ success: false });
        } else {
          res.json({ success: true });
        }
      });
  },
  modelOrder: function (req, res, next) {
    fs.readFile(__root + '/src-api/application/endpoints/skyhub/InterfaceJson/IOrders.json', 'utf8', function (err, data) {
      if (err) throw err;
      obj = JSON.parse(data);
      var replace = "Submarino-" + new Date().getTime();
      var regext = new RegExp(obj.code, "g");
      console.log(regext);
      data = data.replace(regext, replace);
      res.send(data);
    });
  },
  deleteOrder: function (req, res, next) {
    var id = req.params.id;
    req.db.collection("skyhub_orders").deleteOne({ _id: new mongodb.ObjectID(id) }
      , function (err, result) {
        if (err) {
          res.json({ success: false });
        } else {
          res.json({ success: true });
        }
      });
  },
  createOrder: function (req, res, next) {
    var body = req.body;
    body.block = false;
    if (body._id != null) {
      var id = body._id;
      var jsonBody = JSON.parse(body.json);
      jsonBody['x-api-key'] = body.seller;
      req.db.collection("skyhub_orders").updateOne({ _id: new mongodb.ObjectID(id) }
        , { $set: jsonBody }, function (err, result) {
          if (err) {
            res.json({ success: false, msg: err.errmsg });
          } else {
            res.json({ success: true });
          }
        });
    } else {
      delete body._id;
      var jsonBody = JSON.parse(body.json);
      jsonBody['x-api-key'] = body.seller;
      req.db.collection("skyhub_orders").insertOne(jsonBody,
        function (err, result) {
          if (err) {
            res.json({ success: false, msg: err.errmsg });
          } else {
            res.json({ success: true });
          }
        });
    }
  },
  post: function (req, res, next) {
    var json = JSON.parse(req.body.code);
    var headersOpt = {
      "content-type": "application/json",
      "Accept": "application/json"
    };

    var options = {
      uri: 'http://' + req.body.url,
      method: 'POST',
      body: json,
      headers: headersOpt
    }

    request(options, function (error, response, body) {
      if (error) { console.log(error); res.json({ statusCode: 404, body:'Erro na requisição'})}
      res.json({ statusCode: response.statusCode, body: response.body });
    });
  }
}
