var fs = require('fs');
var _ = require('lodash');
module.exports = {
  index: async function (req, res, next) {//get
    /*
     *   Carregar lista de orders dos mkts   

        parameters
        page:number,
        per_page:number max=100,
        filters[sale_system] string,
        filters[statuses][] string

     */
    var params = req.query;
    params.page = (!params.page) ? 1 : parseInt(params.page);
    params.per_page = (!params.per_page) ? 30 : parseInt(params.per_page);
    params["filters[sale_system]"] = (!params["filters[sale_system]"]) ? "" : params["filters[sale_system]"];
    params["filters[statuses][]"] = (!params["filters[statuses][]"]) ? "" : params["filters[statuses][]"];

    var account = req.headers['x-api-key'];
    var count = await req.db.collection("skyhub_orders").count({
      "x-api-key": account
    });

    req.db.collection("skyhub_orders").find({//pegar parametros de busca do skyhub
      "x-api-key": account
    }).skip((params.page - 1) * params.per_page).limit(params.per_page).toArray(function (err, result) {
      var objReponse = {
        total: count,
        orders: result
      };
      res.json(objReponse);
    });
  },
  create: function (req, res, next) {//forma simples de create sku
    var body = req.body;
    var account = req.headers['x-api-key'];
    body['x-api-key'] = account;
    req.db.collection("skyhub_products").insertOne(body,
      function (err, result) {
        if (!err) {
          res.status(200).end();
        } else {
          res.status(404).end();
        }
      });
  },
  update: function (req, res, next) {//forma simples de update sku
    var body = req.body;
    var account = req.headers['x-api-key'];
    body['x-api-key'] = account;
    var sku = req.params.sku;
    req.db.collection("skyhub_products").updateOne({ "product.sku": sku },
      { $set: body },
      function (err, result) {
        console.log(err);
        if (!err) {
          res.status(200).end();
        } else {
          res.status(404).end();
        }
      });
  },
  delete: function (req, res, next) {
    var sku = req.params.sku;
    req.db.collection("skyhub_products").findOne({ "product.sku": sku }, function (err, result) {
      if (result == null) {
        res.status(404).end();
      }
      req.db.collection("skyhub_products").deleteOne({ "product.sku": sku }
        , function (err, result) {
          if (!err) {
            res.status(200).end();
          } else {
            res.status(400).end();
          }
        });
    });
  },
  createVariations: function (req, res, next) {
    var sku = req.params.sku;
    var body = req.body;
    req.db.collection("skyhub_products").findOne({ "product.sku": sku }, function (err, result) {
      if (result) {
        var idx = _.findIndex(result.product.variations, function (r) { return r.sku == body.variation.sku });
        if (idx < 0) {
          //console.log(result);
          result.product.variations.push(body.variation);
          delete result._id;
          req.db.collection("skyhub_products").updateOne({ "product.sku": sku },
            { $set: result },
            function (err, result) {
              console.log(err);
              if (!err) {
                res.status(200).end();
              } else {
                res.status(404).end();
              }
            });
        } else {
          res.status(400).end();
        }
      
      } else {
        res.status(404).end();
      }
    });
  }
}
