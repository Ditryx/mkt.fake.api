var validation = require('../services/validation-service');
module.exports = {
  index: function (req, res, next) {
    /*
     * https://skyhub.gelato.io/docs/versions/1.0
     *
      HEADER
      X-User-Email: email_de_usuario
      X-Api-Key: token_de_integracao
      X-Accountmanager-Key: token_account
      Accept: application/json
      Content-Type: application/json
     *
     *   Authenticador 
     */
    var headers = req.headers;
    var valid = validation.validationAuth(headers, req);
    valid.then(function (vl) {
      if (vl.isValid) {
        next();
      } else {
        res.status(vl.status).json(vl.msg);
      }
    });

	}
}
