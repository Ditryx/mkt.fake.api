var fs = require('fs');
module.exports = {
  index: async function (req, res, next) {//get
    /*
     *   Carregar lista de orders dos mkts   

        parameters
        page:number,
        per_page:number max=100,
        filters[sale_system] string,
        filters[statuses][] string

     */
    var params = req.query;
    params.page = (!params.page) ? 1 : parseInt(params.page);
    params.per_page = (!params.per_page) ? 30 : parseInt(params.per_page);
    params["filters[sale_system]"] = (!params["filters[sale_system]"]) ? "" : params["filters[sale_system]"];
    params["filters[statuses][]"] = (!params["filters[statuses][]"]) ? "" : params["filters[statuses][]"];

    var account = req.headers['x-api-key'];
    var count = await req.db.collection("skyhub_orders").count({
      "x-api-key": account
    });

    req.db.collection("skyhub_orders").find({//pegar parametros de busca do skyhub
      "x-api-key": account
    }).skip((params.page - 1) * params.per_page).limit(params.per_page).toArray(function (err, result) {
      var objReponse = {
        total: count,
        orders: result
      };
      res.json(objReponse);
    });


  },
  order: async function (req, res, next) {//get
    /*
     *  Submarino-1530030713407
     */
    var code = req.params.id;
    var account = req.headers['x-api-key'];
    var result = await req.db.collection("skyhub_orders").findOne({ "code": code, "x-api-key": account});
    if (result) {
      res.json(result);
    } else {
      res.status(404).send("");
    }
  },
  saveOrderTest: async function (req, res, next) {//post

    res.json(req.body);
  },
  shipmentLabels: async function (req, res, next) {
    console.log("AQUII");
    var fileFormat = (req.headers['accept'] == 'application/pdf') ? 'pdf' : 'json';
    var code = req.params.code;
    var account = req.headers['x-api-key'];
    var result = await req.db.collection("skyhub_orders").findOne({ "code": code, "x-api-key": account });
    if (result) {//order existe
      if (fileFormat == 'pdf') {
        var file = __root + '/src-api/application/endpoints/skyhub/custom/orders/shipment_labels.pdf';
        res.download(file);
      } else {
        var data = fs.readFileSync(__root + '/src-api/application/endpoints/skyhub/custom/orders/shipment_labels.json', 'utf8');
        var json = JSON.parse(data);
        res.json(json);
      }
    } else {
      res.status(404).send("");
    }

  }
}
