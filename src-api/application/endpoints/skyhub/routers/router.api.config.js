var express = require('express');
var Router = require(__root + "/src-api/core/router/router.js");
const router = express.Router();
//registro de rotas  /Controller/Action
router.get('/orders', [Router('/auth', 'skyhub'), Router('/orders', 'skyhub')]);
router.get('/orders/:id', [Router('/auth', 'skyhub'), Router('/orders/order', 'skyhub')]);
router.post('/orders', [Router('/auth', 'skyhub'), Router('/orders/create', 'skyhub')]);

//etiquetas
router.get('/orders/:code/shipment_labels', [Router('/auth', 'skyhub'), Router('/orders/shipmentLabels', 'skyhub')]);


//products
router.get('/products', [Router('/auth', 'skyhub'), Router('/products', 'skyhub')]);
router.post('/products', [Router('/auth', 'skyhub'), Router('/products/create', 'skyhub')]);
router.put('/products/:sku', [Router('/auth', 'skyhub'), Router('/products/update', 'skyhub')]);
router.delete('/products/:sku', [Router('/auth', 'skyhub'), Router('/products/delete', 'skyhub')]);
router.post('/products/:sku/variations', [Router('/auth', 'skyhub'), Router('/products/createVariations', 'skyhub')]);

//test order
router.post('/saveOrderTest', Router('/orders/saveOrderTest', 'skyhub'));
module.exports = router;

