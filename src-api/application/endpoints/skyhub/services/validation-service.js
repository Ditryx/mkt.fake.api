var fs = require('fs');
var glob = require('glob');
var _ = require('lodash');
module.exports = {
  validationAuth: async function (headers, req) {
     /*
     * https://skyhub.gelato.io/docs/versions/1.0
     *
      HEADER
      X-User-Email: email_de_usuario
      X-Api-Key: token_de_integracao
      X-Accountmanager-Key: token_account

      Accept: application/json
      Content-Type: application/json

     *
     *   Authenticador 
     */

    var valid = true;
    if (headers['accept'] == '') {
      valid = false;
    }
    else if (headers['content-type'] !== 'application/json' && headers['accept'] !== 'application/pdf') {
      valid = false;
    }
    else if (!headers['x-user-email'] || !headers['x-api-key'] || !headers['x-accountmanager-key']) {
      valid = false;
    } else {
      valid = false;
      var result = await req.db.collection('skyhub_sellers').findOne({
        "x-user-email": headers['x-user-email'],
        "x-api-key": headers['x-api-key'],
        "x-accountmanager-key": headers['x-accountmanager-key']
      });

      if (result) {
        valid = true;
      }
    }
    return {
      isValid: valid, status: 400, msg:
        { error: "mensagem de erro" }
    }
  }
}
