import { HttpModule } from "@angular/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatButtonModule, MatCheckboxModule, MatSidenavModule, MatMenuModule, MatIconModule, MatCardModule, MatDividerModule, MatTableModule, MatDialogModule, MatSelectModule, MatPaginatorModule, MatInputModule } from "@angular/material";
import { RouterModule } from "@angular/router";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { NavComponent } from "./components/nav/nav.component";
import { MonacoEditorModule, NgxMonacoEditorConfig } from 'ngx-monaco-editor';
@NgModule({
  imports: [
    HttpModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatMenuModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    MatTableModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule.forRoot(),
    MatDialogModule,
    MatSelectModule,
    MatPaginatorModule,
    MatInputModule,
    MonacoEditorModule.forRoot() 
  ],
  declarations: [NavComponent],
  exports: [
    NavComponent,
    HttpModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatMenuModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    MatTableModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    MatDialogModule,
    MatSelectModule,
    MatPaginatorModule,
    MatInputModule,
    MonacoEditorModule],
  providers: []
})
export class SharedModule {
}
