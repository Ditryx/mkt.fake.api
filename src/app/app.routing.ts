import {Routes, RouterModule} from "@angular/router";
import {ModuleWithProviders, NgModule} from "@angular/core";
import { DashboardComponent } from "./features/dashboard/app.dashboard.component";

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )],
  exports: [RouterModule]
})

export class AppRoutingModule { }

