import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from "@angular/http";
import 'rxjs';

import { Constants } from "../../../app.constants.global";

@Injectable()
export class DashboardService {
  constructor(private http: Http) {
  }
  get() {
    return this.http
      .get(Constants.API_ENDPOINT + '/dashboard', {})
      .toPromise()
      .then(response => {
        if (response.json().success) {
          return response.json();
        } else {
          return response.json();
        }
      })
  }
}
