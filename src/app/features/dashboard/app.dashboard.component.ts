import { Component, OnInit } from '@angular/core';
import { DashboardService } from './services/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './app.dashboard.component.html',
  styleUrls: ['./app.dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  title = 'app';
  apis: any;
  constructor(private service: DashboardService) {
    this.service.get().then(x => {
      this.apis = x;
    });
  }
  ngOnInit() { }

}
