import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class SkyHubProductsComponent implements OnInit {
  title = 'Sellers'; 
  displayedColumns = ['x-user-email', 'x-api-key', 'x-accountmanager-key"'];
  dataSource: MatTableDataSource<SellerElement>;
  @ViewChild(MatSort) sort: MatSort;
  ngOnInit() { this.dataSource.sort = this.sort; }
}

export interface SellerElement {
  "x-user-email": string;
  "x-api-key": number;
  "x-accountmanager-key": number;
}
