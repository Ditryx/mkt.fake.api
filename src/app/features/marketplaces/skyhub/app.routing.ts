import {Routes, RouterModule} from "@angular/router";
import {ModuleWithProviders, NgModule} from "@angular/core";
import { SkyHubSellersComponent } from "./sellers/sellers.component";
import { SkyHubOrdersComponent, SkyHubDialogOrderCreate, SkyHubDialogOrderPost } from "./orders/orders.component"; 
import { SkyHubSellersEditComponent} from "./sellers/sellers-edit.component";
import { SkyHubProductsComponent } from "./products/products.component";

const appRoutes: Routes = [{
    path: 'skyhub/sellers',
    component: SkyHubSellersComponent
  },
  {
    path: 'skyhub/sellers/edit',
    component: SkyHubSellersEditComponent
  },
  {
    path: 'skyhub/sellers/edit/:apiKey',
    component: SkyHubSellersEditComponent
  },
  {
    path: 'skyhub/orders',
    component: SkyHubOrdersComponent
  }

];
@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule]
})
export class SkyHubRoutingModule { }
 
export const routedComponents = [
  SkyHubSellersComponent,
  SkyHubSellersEditComponent,
  SkyHubOrdersComponent,
  SkyHubDialogOrderCreate,
  SkyHubDialogOrderPost,
  SkyHubProductsComponent
];

export const entryCompenents = [
  SkyHubDialogOrderCreate,
  SkyHubDialogOrderPost
];


