import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from "@angular/http";
import 'rxjs';

import { Constants } from "../../../../../app.constants.global";

@Injectable()
export class SkyHubSellersService {
  constructor(private http: Http) {
  }
  get() {
    return this.http
      .get(Constants.API_ENDPOINT + '/skyhub/sellers', {})
      .toPromise()
      .then(response => {
        if (response.json().success) {
          return response.json();
        } else {
          return response.json();
        }
      })
  };

  edit(apiKey) {
    return this.http
      .get(Constants.API_ENDPOINT + '/skyhub/sellers/' + apiKey, {})
      .toPromise()
      .then(response => {
        if (response.json().success) {
          return response.json();
        } else {
          return response.json();
        }
      })
  }

  saveSeller(obj) {
    return this.http
      .post(Constants.API_ENDPOINT + '/skyhub/sellers/create', obj)
      .toPromise()
      .then(response => {
        if (response.json()) {
          return response.json();
        } else {
          return response.json();
        }
      });
  }

  deleteSeller(id) {
    return this.http
      .delete(Constants.API_ENDPOINT + '/skyhub/sellers/delete/'+id)
      .toPromise()
      .then(response => {
        if (response.json()) {
          return response.json();
        } else {
          return response.json();
        }
      });
  }
}
