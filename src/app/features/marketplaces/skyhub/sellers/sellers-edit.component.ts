import { Component, OnInit, ViewChild } from '@angular/core';
import { SkyHubSellersService } from './service/sellers.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-sellers-edit',
  templateUrl: './sellers-edit.component.html',
  styleUrls: []
})
export class SkyHubSellersEditComponent implements OnInit {
  title = 'Sellers';
  seller: any = [];
  form: FormGroup;
  constructor(private service: SkyHubSellersService,
    private ActivatedRoute: ActivatedRoute,
    private router: Router, fb: FormBuilder) {
    this.form = fb.group({
      'x-user-email': [null, Validators.required],
      'x-api-key': [null, Validators.required],
      'x-accountmanager-key': [null, Validators.required],
      'urlSaveOrder': [null, Validators.required],
      '_id': [null],
      'block': [null]
    });
  }
  ngOnInit() {
    this.ActivatedRoute.params.subscribe(params => {
      var apiKey = (params && params['apiKey'] != '') ? params['apiKey'] : null;
      if (apiKey) {
        this.service.edit(apiKey).then(r => {
          this.form.setValue(r.seller);
        });
      }
    });
  }

  save(value: any) {
    if (this.form.valid) {
      this.service.saveSeller(value).then(r => {
        if (r.success) {
          this.router.navigate(['/skyhub/sellers']);
        } else {
          alert('Error ao salvar!');
        }
      })
    }
  }
}
