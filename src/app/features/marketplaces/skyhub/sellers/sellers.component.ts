import { Component, OnInit, ViewChild } from '@angular/core';
import { SkyHubSellersService } from './service/sellers.service';
@Component({
  selector: 'app-sellers',
  templateUrl: './sellers.component.html',
  styleUrls: ['./sellers.component.css']
})
export class SkyHubSellersComponent implements OnInit {
  title = 'Sellers';
  sellers: any = [];
  constructor(private service: SkyHubSellersService) {
    this.list();
  }
  ngOnInit() { }


  list() {
    this.service.get().then(x => {
      this.sellers = x;
    });
  }
  delete(id) {
    if (confirm("Você tem certeza que deseja excluir o registro?")) {
      this.service.deleteSeller(id).then(r => {
        if (r.success) {
          this.list();
        } else {
          alert('Error ao excluir registro!');
        }
      });
    }
  }
}
