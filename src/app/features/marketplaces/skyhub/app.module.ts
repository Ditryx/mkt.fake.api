import { NgModule } from '@angular/core';
import { SkyHubRoutingModule, routedComponents, entryCompenents } from './app.routing';
import { SkyHubSellersService } from './sellers/service/sellers.service'; 
import { SharedModule } from '../../../shared/app.module';
import { SkyHubOrdersService } from './orders/service/orders.service';
@NgModule({
  imports: [
    SharedModule,
    SkyHubRoutingModule
  ],
  declarations: routedComponents,
  entryComponents: entryCompenents,
  exports: [],
  providers: [
    SkyHubSellersService,
    SkyHubOrdersService
  ]
})
export class SkyHubModule {
}
