import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { SkyHubOrdersService } from './service/orders.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, PageEvent } from '@angular/material';
import { NgxEditorModel } from 'ngx-monaco-editor';
import { SkyHubSellersService } from '../sellers/service/sellers.service';
@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class SkyHubOrdersComponent implements OnInit {
  orders: any = [];
  sellers: any = [];
  pagination: any = { length: 0, pageSize: 10, pageSizeOptions:[5, 10, 25, 100], page:0 };
  filter: any = { seller: null, code: null, customer: null, date: null };
  constructor(private service: SkyHubOrdersService, private sellerService: SkyHubSellersService, public dialog: MatDialog) {
    this.list();
    this.sellerService.get().then(x => {
      this.sellers = x;
    });
    
  }
  ngOnInit() { }

  changePage(event) {
    console.log(event);
    this.pagination.pageSize = event.pageSize;
    this.pagination.page = event.pageIndex;
    this.list();
  }
  list() {
    var filter = this.filter;
    filter.page = this.pagination.page;
    filter.per_page = this.pagination.pageSize;
    this.service.get(filter).then(x => {
      this.pagination.length = x.count;
      this.orders = x.list;
    });
  }

  openDialog() {
    const dialogRef = this.dialog.open(SkyHubDialogOrderCreate, {
      height: '99%',
      width: '99%',
      data: { sellers: this.sellers, order:null }
    });

    dialogRef.afterClosed().subscribe(result => {
        //close
      this.list();
    });


  }

  delete(id) {
    if (confirm("Você tem certeza que deseja excluir o registro?")) {
      this.service.delete(id).then(r => {
        if (r.success) {
          this.list();
        } else {
          alert('Error ao excluir registro!');
        }
      });
    }
  }

  editOrder(op) {
    const dialogRef = this.dialog.open(SkyHubDialogOrderCreate, {
      height: '99%',
      width: '99%',
      data: { sellers: this.sellers, order: op }
    });

    dialogRef.afterClosed().subscribe(result => {
      //close
      this.list();
    });
  }

  postOrder(op) {
    const dialogRef = this.dialog.open(SkyHubDialogOrderPost, {
      height: '99%',
      width: '99%',
      data: { order: op }
    });

    dialogRef.afterClosed().subscribe(result => {
      //close
      this.list();
    });
  }
}
@Component({
  selector: 'orders-create-dialog',
  templateUrl: 'orders-create.component.html'
})
export class SkyHubDialogOrderCreate {
  sellers: any = [];
  seller: any;
  optionsMonaco: any = { theme: 'vs-dark' };
  valueModel: any;
  code: any;
  _id: any = null;
  model: NgxEditorModel = {
    value:"",
    language: 'json'
  };

  constructor(
    public dialogRef: MatDialogRef<any>,
    public service: SkyHubOrdersService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.sellers = data.sellers;
    
    if (data.order != null) {
      this.seller = data.order['x-api-key'];
      this._id = data.order._id;
      delete data.order._id;
      delete data.seller;
      setTimeout(x => {
        this.code = JSON.stringify(data.order).replace(/,/g, ',\n');
      }, 1000);
      
    }
    this.seller = (this.sellers.length > 0) ? this.sellers[0]['x-api-key'] : null;
  }
    onNoClick(): void {
      this.dialogRef.close();
    }

  loadModel() {
    this.service.model().then(x => {
      this.code = x;
    });
  }
  saveOrder() { // create
    if (this.code != "") {
      try {
        var jsonTeste = JSON.parse(this.code);
        var obj = { seller: this.seller, json: this.code, _id: this._id };
        this.service.save(obj).then(x => {
          if (x.success) {
            alert('Salvo com sucesso!');
            this.code = '';
            this.dialogRef.close();
          } else {
            alert(x.msg);
          }
        })
      } catch(err) {
        alert('Por favor, insira um código json válido');
      }
    } else {
      alert('Por favor, insira um código json');
    }
  }
}

@Component({
  selector: 'orders-post-dialog',
  templateUrl: 'orders-post.component.html'
})
export class SkyHubDialogOrderPost {
  optionsMonaco: any = { theme: 'vs' };
  code: any;
  codeResponse: any;
  urlSend: any;
  order: any = null;
  response: any;
  model: NgxEditorModel = {
    value: "",
    language: 'json'
  };
  model2: NgxEditorModel = {
    value: "",
    language: 'json'
  };

  constructor(
    public dialogRef: MatDialogRef<any>,
    public service: SkyHubOrdersService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    var refOrder = Object.assign({}, data.order);
    this.order = Object.assign({}, refOrder);
    
    if (refOrder.seller.length > 0) {
      this.order.seller = refOrder.seller[0];
      this.urlSend = this.order.seller.urlSaveOrder;
    }
    delete refOrder.seller;
    delete refOrder._id;
    setTimeout(x => {
      this.code = JSON.stringify(refOrder).replace(/,/g, ',\n');
    }, 1000);
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  send() {
    if (this.urlSend != '') {
      var obj = { url: this.urlSend, code: JSON.stringify(this.code) };
      this.service.send(obj).then(r => {
        this.response = 'StatusCode:' + r.statusCode;
        this.codeResponse = r.body.replace(/,/g, ',\n');
      });
    }
  }
}
