import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from "@angular/http";
import 'rxjs';

import { Constants } from "../../../../../app.constants.global";

@Injectable()
export class SkyHubOrdersService {
  constructor(private http: Http) {
  }
  get(obj: any) {
    obj
    return this.http
      .get(Constants.API_ENDPOINT + '/skyhub/orders', { params: obj })
      .toPromise()
      .then(response => {
        if (response.json().success) {
          return response.json();
        } else {
          return response.json();
        }
      })
  };

  edit(id) {
    return this.http
      .get(Constants.API_ENDPOINT + '/skyhub/orders/' + id, {})
      .toPromise()
      .then(response => {
        if (response.json().success) {
          return response.json();
        } else {
          return response.json();
        }
      })
  }

  save(obj) {
    return this.http
      .post(Constants.API_ENDPOINT + '/skyhub/orders/create', obj)
      .toPromise()
      .then(response => {
        if (response.json()) {
          return response.json();
        } else {
          return response.json();
        }
      });
  }
  send(obj) {
    return this.http
      .post(Constants.API_ENDPOINT + '/skyhub/orders/post', obj)
      .toPromise()
      .then(response => {
        if (response.json()) {
          return response.json();
        } else {
          return response.json();
        }
      });
  }

  delete(id) {
    return this.http
      .delete(Constants.API_ENDPOINT + '/skyhub/orders/delete/'+id)
      .toPromise()
      .then(response => {
        if (response.json()) {
          return response.json();
        } else {
          return response.json();
        }
      });
  }

  model() {
    return this.http
      .get(Constants.API_ENDPOINT + '/skyhub/orders/model', {})
      .toPromise()
      .then(response => {
        if (response) {
          return response['_body'];
        }
      })
  }
}
