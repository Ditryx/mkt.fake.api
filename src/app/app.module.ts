import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DashboardComponent } from './features/dashboard/app.dashboard.component';
import { AppRoutingModule } from './app.routing';

import { DashboardService } from './features/dashboard/services/dashboard.service';

import { SkyHubModule } from './features/marketplaces/skyhub/app.module';
import { SharedModule } from './shared/app.module';



@NgModule({
  imports: [
    SharedModule,
    AppRoutingModule,//routes gerais sem module
    SkyHubModule
  ],
  declarations: [
    AppComponent,
    DashboardComponent
  ],
  providers: [DashboardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
