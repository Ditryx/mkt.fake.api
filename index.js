/**
 * Created by alexandre on 06/09/17.
 */
var path = require('path');
var vhost = require('vhost');
var debug = require('debug');
var express = require('express');
var _ = require('lodash');
global.__root = path.resolve(__dirname).replace(/\\/g, '/');
global.constants = require(__root + '/src-api/application/helpers/constants.js');
global.helpers = require(__root + '/src-api/application/helpers/helpers.js');
const app = express();


var apps = require(__root + '/src-api/core/apps');// -> AQUI CONFIGURACAO GERAL DA APLICACAO
app.use(express.static(path.resolve('dist')));
app.use('/node_modules', express.static(__root + '/node_modules'));

_.forEach(apps, function (r) {
  app.use(vhost(r.host, r.app));
});


app.listen(80);



